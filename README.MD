![introduction](introduction.jpg)
![sort](sort.jpg)
![filter map](filter_map.jpg)
![All Any and other predicates](all_any_and_other_predicates.jpg)
![associate](associate.jpg)
![groupBy](groupBy.jpg)
![partition](partition.jpg)
![flatMap](flatmMap.jpg)
![maxmin](maxmin.jpg)
![sum](sum.jpg)
![fold and reducte](fold_and_reduce.jpg)
![compound tasks](compound_tasks.jpg)
![sequences](sequences.jpeg)
![getting used to new style](getting_used_to_new_style.jpeg)



