// Return the set of products that were ordered by all customers
fun Shop.getProductsOrderedByAll(): Set<Product> =
    this.customers.map{ it.getOrderedProducts() }
    	.reduce{orderedProducts, products -> orderedProducts.intersect(products)}


fun Customer.getOrderedProducts(): Set<Product> =
    this.orders.flatMap(Order::products).toSet()
    
